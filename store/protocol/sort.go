package protocol

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type byExecutionDate []test.CaseExecutionProtocol

func (bed byExecutionDate) Len() int {
	return len(bed)
}

func (bed byExecutionDate) Less(i, j int) bool {
	return bed[i].ExecutionDate.Before(bed[j].ExecutionDate)
}

func (bed byExecutionDate) Swap(i, j int) {
	bed[i], bed[j] = bed[j], bed[i]
}
