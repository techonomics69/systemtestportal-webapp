/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package comment

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

type ramStore struct {
	sync.RWMutex
	comments map[string]map[string]map[string]map[bool][]*comment.Comment
}

func initRAMStore() *ramStore {
	return &ramStore{comments: make(map[string]map[string]map[string]map[bool][]*comment.Comment)}
}

func (s *ramStore) Add(c *comment.Comment) error {
	if s == nil {
		panic("trying to add a comment to a nil store")
	}
	if c == nil {
		panic("trying to add nil comment to the store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.comments[c.ID.Owner()]
	if !ok {
		pm = make(map[string]map[string]map[bool][]*comment.Comment)
		s.comments[c.ID.Owner()] = pm
	}

	om, ok := pm[c.ID.Project()]
	if !ok {
		om = make(map[string]map[bool][]*comment.Comment)
		pm[c.ID.Project()] = om
	}

	tm, ok := om[c.ID.Test()]
	if !ok {
		tm = make(map[bool][]*comment.Comment)
		om[c.ID.Test()] = tm
	}

	tm[c.ID.IsCase()] = append(tm[c.ID.IsCase()], c)
	return nil
}

func (s *ramStore) Get(id id.TestID) ([]*comment.Comment, bool) {
	if s == nil {
		panic("trying to retrieve a comment from a nil store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.comments[id.Owner()]
	if !ok {
		return nil, false
	}

	om, ok := pm[id.Project()]
	if !ok {
		return nil, false
	}

	tm, ok := om[id.Test()]
	if !ok {
		return nil, false
	}

	c, ok := tm[id.IsCase()]
	return c, ok
}
