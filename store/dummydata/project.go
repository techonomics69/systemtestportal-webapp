// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

// Projects contains the dummy projects
// of the default configuration of the system
var Projects = []project.Project{
	{
		Owner:       Users[0].ID(),
		Name:        "DuckDuckGo.com",
		Description: "Test the search engine DuckDuckGo.com",
		Visibility:  visibility.Public,
		Variants: map[string]*project.Variant{
			"Chrome": {
				Name: "Chrome",
				Versions: []project.Version{
					{Name: "v0.2"},
					{Name: "v0.3.1"},
					{Name: "v0.4.6"},
					{Name: "v42.5.25"},
				},
			},

			"Firefox": {
				Name: "Firefox",
				Versions: []project.Version{
					{Name: "v0.1.11"},
					{Name: "v0.2.49"},
					{Name: "v0.4.0"},
					{Name: "v0.4.6"},
				},
			},

			"Microsoft Edge": {
				Name: "Microsoft Edge",
				Versions: []project.Version{
					{Name: "v0.175"},
					{Name: "v0.204"},
					{Name: "v1000"},
					{Name: "v1019"},
				},
			},
		},
		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
				Color:       "ff0000",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
				Color:       "ff7b00",
			},
			{
				Name:        "Do Later",
				Description: "This test are to be done later",
				Color:       "ffc107",
			},
			{
				Name:        "Ignore",
				Description: "Ignore these tests",
				Color:       "b10dff",
			},
			{
				Name:        "Blue label",
				Description: "This is a blue label",
				Color:       "007bff",
			},
		},
		Roles: Roles,
		UserMembers: map[id.ActorID]project.UserMembership{
			id.ActorID("default"): {
				User:        "default",
				Role:        "Supervisor",
				MemberSince: time.Now().Round(time.Second),
			},
			id.ActorID("admin"): {
				User:        "admin",
				Role:        "Supervisor",
				MemberSince: time.Now().Round(time.Second),
			},
			id.ActorID("alexanderkaiser"): {
				User:        "alexanderkaiser",
				Role:        "Tester",
				MemberSince: time.Now().Round(time.Second),
			},
			id.ActorID("simoneraab"): {
				User:        "simoneraab",
				Role:        "Manager",
				MemberSince: time.Now().Round(time.Second),
			},
		},
	}, {
		Owner:       Users[2].ID(),
		Name:        "SystemTestPortal",
		Description: "Tests the web application \"SystemTestPortal\".",
		Visibility:  visibility.Public,
		Variants: map[string]*project.Variant{
			"Chrome": {
				Name: "Chrome",
				Versions: []project.Version{
					0: {Name: "v0.12.0"},
				},
			},
			"Firefox": {
				Name: "Firefox",
				Versions: []project.Version{
					0: {Name: "v0.12.0"},
				},
			},
			"Microsoft Edge": {
				Name: "Microsoft Edge",
				Versions: []project.Version{
					0: {Name: "v0.12.0"},
				},
			},
		},
		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
			},
		},
		Roles:        Roles,
		CreationDate: time.Now().Round(time.Second),
	},
}

// Labels are dummy labels for testing
var Labels = []project.Label{
	{
		Name:        "High Priority",
		Description: "This test has a high priority",
		Color:       "ff0000",
	},
	{
		Name:        "Low Priority",
		Description: "This test has a low priority",
		Color:       "ff7b00",
	},
	{
		Name:        "Elephant",
		Description: "This test has an elephant priority",
		Color:       "787d84",
	},
	{
		Name:        "Verschiedene",
		Description: "Huehuehuehue",
		Color:       "518754",
	},
	{
		Name:        "Obstsalat",
		Description: "Schlupp",
		Color:       "7c4596",
	},
}

// UserMembers are dummy userships for testing
var UserMembers = map[id.ActorID]project.UserMembership{
	id.ActorID("default"): {
		User:        "default",
		Role:        "Supervisor",
		MemberSince: time.Now().UTC().Round(time.Second),
	},
	id.ActorID("admin"): {
		User:        "admin",
		Role:        "Supervisor",
		MemberSince: time.Now().UTC().Round(time.Second),
	},
	id.ActorID("alexanderkaiser"): {
		User:        "alexanderkaiser",
		Role:        "Tester",
		MemberSince: time.Now().UTC().Round(time.Second),
	},
	id.ActorID("simoneraab"): {
		User:        "simoneraab",
		Role:        "Manager",
		MemberSince: time.Now().UTC().Round(time.Second),
	},
}

// Roles are dummy roles for testing
var Roles = map[project.RoleName]*project.Role{
	"Supervisor": {
		Name: "Supervisor",
		Permissions: project.Permissions{
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: true,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    true,
				EditSequence:      true,
				DeleteSequence:    true,
				DuplicateSequence: true,
				AssignSequence:    true,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: true,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     true,
				DeleteProject:   true,
				EditPermissions: true,
			},
		},
	},
	"Manager": {
		Name: "Manager",
		Permissions: project.Permissions{
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: true,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    true,
				EditSequence:      true,
				DeleteSequence:    true,
				DuplicateSequence: true,
				AssignSequence:    true,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: false,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     false,
				DeleteProject:   false,
				EditPermissions: false,
			},
		},
	},
	"Tester": {
		Name: "Tester",
		Permissions: project.Permissions{
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: true,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    false,
				EditCase:      false,
				DeleteCase:    false,
				AssignCase:    false,
				DuplicateCase: false,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    false,
				EditSequence:      false,
				DuplicateSequence: false,
				DeleteSequence:    false,
				AssignSequence:    false,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: false,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     false,
				DeleteProject:   false,
				EditPermissions: false,
			},
		},
	},
}

var browserVariants = map[string]*project.Variant{
	"Chrome": {
		Name: "Chrome",
		Versions: []project.Version{
			0: {Name: "v0.12.0"},
		},
	},
	"Firefox": {
		Name: "Firefox",
		Versions: []project.Version{
			0: {Name: "v0.12.0"},
		},
	},
	"Microsoft Edge": {
		Name: "Microsoft Edge",
		Versions: []project.Version{
			0: {Name: "v0.12.0"},
		},
	},
}
