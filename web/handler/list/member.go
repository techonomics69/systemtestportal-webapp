/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"net/http"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// MembersGet simply serves a page that is used to list the members of a project,
func MembersGet(lister handler.ProjectLister, us middleware.UserRetriever, ul handler.UserLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		users, err := ul.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		nms := nonMembers(users, c.Project.UserMembers)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		b := false

		if c.User != nil {
			if strings.Compare(c.Project.Owner.Actor(), c.User.Name) == 0 {
				b = true
			}
		}

		tmpl := getMemberListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.NonMember, nms).
			With(context.Member, c.Project.UserMembers).
			With(context.Owner, b), tmpl, w, r)
	}
}

// getTestCaseListFragment returns either only the test case list fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getMemberListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabMembersListFragment()
	}
	return getTabMembersListTree()
}

// getTabMembersListTree returns the members template with all parent templates
func getTabMembersListTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab members tree
		Append(templates.Members).
		Append(templates.AssignMember).
		Append(templates.RemoveMember).
		Get().Lookup(templates.HeaderDef)
}

// getTabMembersListFragment returns only the protocol list tab template
func getTabMembersListFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.Members).
		Append(templates.AssignMember).
		Append(templates.RemoveMember).
		Get().Lookup(templates.TabContent)

}

func nonMembers(us []*user.User, ms map[id.ActorID]project.UserMembership) []*user.User {
	var nms []*user.User
	for _, u := range us {
		if _, ok := ms[u.ID()]; ok {
			continue
		}
		nms = append(nms, u)
	}

	return nms
}
