/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"bytes"
	"encoding/json"
	"time"

	"log"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
)

// DummyProject contains a project for testing purposes
var DummyProject = &project.Project{
	Name:        "DuckDuckGo.com",
	Description: "Test the online search engine DuckDuckGo.com",
	Visibility:  visibility.Public,
	Owner:       "default",
	Variants: map[string]*project.Variant{
		"Chrome": {
			Name: "Chrome",
			Versions: []project.Version{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Firefox": {
			Name: "Firefox",
			Versions: []project.Version{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Microsoft Edge": {
			Name: "Microsoft Edge",
			Versions: []project.Version{
				0: {Name: "1.0.2"},
				1: {Name: "2.0.2"},
				2: {Name: "3.0.1"},
			},
		},
	},
	UserMembers: map[id.ActorID]project.UserMembership{
		id.ActorID(DummyUser.Name): *DummyMembership,
	},
	Roles:        dummydata.Roles,
	CreationDate: time.Now().UTC().Round(time.Second),
}

// DummyTestCase is a test case for testing purposes
var DummyTestCase = &test.Case{
	Name: "Test Case 1",
	TestCaseVersions: []test.CaseVersion{
		{
			Description: "First Test Case",
			Variants:    map[string]*project.Variant{},
			Steps: []test.Step{
				{
					Index:          1,
					Action:         "Open the application",
					ExpectedResult: "The main screen should show",
				},
				{
					Index:          2,
					Action:         "Open the login page",
					ExpectedResult: "The login page should show",
				},
				{
					Index:          3,
					Action:         "Login with the test login",
					ExpectedResult: "The login should succeed",
				},
			},
		},
	},
	Project: DummyProject.ID(),
}

// DummyTestCaseSUTVersions is a test case for testing purposes
// that contains sut-versions
var DummyTestCaseSUTVersions = &test.Case{
	Name: "Test Case 1",
	TestCaseVersions: []test.CaseVersion{
		{
			Description: "First Test Case",
			Variants: map[string]*project.Variant{
				"Chrome": {
					Name: "Chrome",
					Versions: []project.Version{
						0: {Name: "1.0.1"},
						1: {Name: "2.0.1"},
						2: {Name: "3.0.5"},
					},
				},
				"Firefox": {
					Name: "Firefox",
					Versions: []project.Version{
						0: {Name: "1.0.1"},
						1: {Name: "2.0.1"},
						2: {Name: "3.0.5"},
					},
				},
				"Microsoft Edge": {
					Name: "Microsoft Edge",
					Versions: []project.Version{
						0: {Name: "1.0.2"},
						1: {Name: "2.0.2"},
						2: {Name: "3.0.1"},
					},
				},
			},
			Steps: []test.Step{
				{
					Index:          1,
					Action:         "Open the application",
					ExpectedResult: "The main screen should show",
				},
				{
					Index:          2,
					Action:         "Open the login page",
					ExpectedResult: "The login page should show",
				},
				{
					Index:          3,
					Action:         "Login with the test login",
					ExpectedResult: "The login should succeed",
				},
			},
		},
	},
	Project: DummyProject.ID(),
}

// DummyUser is a user for testing purposes
var DummyUser = &user.User{
	DisplayName: "DisplayName",
	Name:        "acc_name",
	EMail:       "test@example.com",
}

// DummyUserUnauthorized is a user that
// is is not authorized to send requests
// because he is not a member of a project
var DummyUserUnauthorized = &user.User{
	DisplayName: "DisplayName",
	Name:        "unauthorized",
	EMail:       "un@authorized.com",
}

// DummyMembership represents a membership that is used for testing purposes
// The user has the role "Supervisor" so he ca
var DummyMembership = &project.UserMembership{
	User:        "acc_name",
	Role:        "Supervisor",
	MemberSince: time.Now().UTC().Round(time.Second),
}

// DummyGroup is a group for testing purposes
var DummyGroup = &group.Group{
	Name:        "Dummy Group Name",
	Description: "Desc",
	Visibility:  visibility.Public,
}

var dummyTestSequenceVersion = test.SequenceVersion{
	VersionNr:     1,
	Description:   "-",
	Preconditions: "-",
	SequenceInfo: test.SequenceInfo{
		Variants:      map[string]*project.Variant{},
		DurationHours: 0,
		DurationMin:   0,
	},
	Cases: []test.Case{
		*DummyTestCase,
		*DummyTestCase,
	},
	CreationDate: time.Now().Round(time.Second),
	IsMinor:      false,
	Message:      "-",
}

// DummyTestSequence is a sequence for testing purposes
var DummyTestSequence = &test.Sequence{
	Name:   "test-sequence-1",
	Labels: []project.Label{},
	SequenceVersions: []test.SequenceVersion{
		dummyTestSequenceVersion,
	},
}

// DummyTestSequenceJSON is sequence in json for testing purposes
var DummyTestSequenceJSON string

func init() {
	b := bytes.NewBufferString("")
	err := json.NewEncoder(b).Encode(DummyTestSequence)
	if err != nil {
		log.Fatalf("Could not encode dummy test sequence as json.\n%v", DummyTestSequence)
	}
	DummyTestSequenceJSON = b.String()
}
