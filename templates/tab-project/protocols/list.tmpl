{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Protocol List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            <ol>
                                <li>Choose test cases or test sequences to display the according protocols.</li>
                                <li>Select one of the existing cases or sequences.</li>
                                <li>Click on a row to display the protcol.</li>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Filters</h5></td>
                    </tr>
                    <tr>
                        <td><strong>Result</strong></td>
                        <td>Hide and display protocols with the individual result.</td>
                    </tr>
                    <tr>
                        <td><strong>System Variant</strong></td>
                        <td>Show only protocols with a specific system variant.</td>
                    </tr>
                    <tr>
                        <td><strong>System Version</strong></td>
                        <td>Show only protocols with a specific system version.</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestProtocols">
    <nav class="navbar navbar-light action-bar p-3 d-print-none">
        <div class="input-group flex-nowrap">
        {{ $selectedType := .SelectedType }}
            <div class="mr-2 form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="protocolType" id="testCases" value="testcases"
                           onclick="typeSelection()" {{ if eq "testcases" $selectedType }} checked {{ end }}> Test Cases
                </label>
            </div>
            <div class="mr-2 form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="protocolType" id="testsequences"
                           value="testsequences" onclick="typeSelection()" {{ if eq "testsequences" $selectedType }}
                           checked {{ end }}> Test Sequences
                </label>
            </div>
            <select class="mr-2 d-none custom-select" id="inputSelectTestCase" onChange="caseSelection();">
                <option class="dropdown-item" value="none" selected>Select ...</option>
            {{ $selected := .SelectedTestCase }}
            {{ range .TestCases }}
                <option class="dropdown-item" value="{{ .Name }}" {{ if eq .Name $selected }}
                        selected {{ end }}>{{ .Name }}</option>
            {{ end }}
            </select>
            <select class="mr-2 d-none custom-select" id="inputSelectTestSequence" onChange="sequenceSelection();">
                <option class="dropdown-item" value="none" selected>Select ...</option>
            {{ $selected := .SelectedTestSequence }}
            {{ range .TestSequences }}
                <option class="dropdown-item" value="{{ .Name }}" {{ if eq .Name $selected }}
                        selected {{ end }}>{{ .Name }}</option>
            {{ end }}
            </select>
        </div>
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-9 p-3">
            <h4 class="mb-3">Protocols</h4>
            <table class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th style="width:10%;">Result</th>
                    <th style="width:25%;">System Variant</th>
                    <th style="width:25%;">System Version</th>
                    <th style="width:40%;">Date</th>
                </tr>
                </thead>
                <tbody id="protocolTable">
                <tr>
                    <td colspan="5" class="text-center">... Loading ...</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3 p-3 tab-side-bar d-print-none">
            <div class="form-row">
                <div class="col-12">
                    <h4>
                        Filter
                    </h4>
                </div>
                <div class="col-12">
                    <strong>Result</strong>
                </div>
                <div class="col-12">
                    <div class="form-check">
                        <label class="form-check-label text-success">
                            <input id="filterSuccessfulTest" name="filterTestResults"
                                   class="form-check-input" type="checkbox"
                                   value="Pass" onchange="updateFilter();" checked/>
                            Passed
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label text-warning">
                            <input id="filterPassWithCommentTest" name="filterTestResults"
                                   class="form-check-input" type="checkbox"
                                   value="PassWithComment" onchange="updateFilter();" checked/>
                            Passed with comment
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label text-danger">
                            <input id="filterFailedTest" name="filterTestResults"
                                   class="form-check-input" type="checkbox"
                                   value="Fail" onchange="updateFilter();" checked/>
                            Failed
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label text-muted">
                            <input id="filterNotAssessedTest" name="filterTestResults"
                                   class="form-check-input" type="checkbox"
                                   value="NotAssessed" onchange="updateFilter();" checked/>
                            Not assessed
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-row" data-toggle="tooltip" data-placement="bottom" title=""
                 data-original-title="This feature will be available in a future version">
                <div class="col-12">
                    <strong>System Variant</strong>
                </div>
                <div class="col-12">
                    <select class="mt-2 custom-select" id="inputSelectTestVariant" onChange="updateFilter();">
                        <option class="dropdown-item" value="all">all</option>
                        <!--TODO: Add Variants-->
                    </select>
                </div>
            </div>
            <div class="form-row" data-toggle="tooltip" data-placement="bottom" title=""
                 data-original-title="This feature will be available in a future version">
                <div class="col-12">
                    <strong>System Version</strong>
                </div>
                <div class="col-12">
                    <select class="mt-2 custom-select" id="inputSelectTestVersion" onChange="updateFilter();">
                        <option class="dropdown-item" value="all">all</option>
                        <!--TODO: Add Versions-->
                    </select>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/project/testprotocols.js"></script>
    <script src="/static/js/project/project-tabs.js"></script>
    <script>
    $(document).ready(function () {
            typeSelection();
            caseSelection();
        });

        $("#printerIcon").removeClass("d-none");

        function printer() {
            console.log("Druckvorschau geladen");
            printpr();
        }
    </script>
</div>
{{end}}


