{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

// This script is not used currently. It will be used in future versions!

// $.getScript("/static/js/util/common.js");
// $.getScript("/static/js/util/ajax.js");
//
// var testCases = [];
// var testVersions = [];
// var tr, th, td, tn, table;
// //var testResults;
//
//
// // creates table and fills it with data
// function createTable() {
//     table = $("#dashboardTable");
//     tr = document.createElement('tr');
//     tr.appendChild(document.createElement('th'));
//     for(var testVersion in testVersions){
//         th = document.createElement('th');
//         tn = document.createTextNode(testVersion);
//         th.appendChild(tn);
//         tr.appendChild(th);
//     }
//     table.appendChild(tr);
//
//     for(var testCase in testCases){
//         tr = document.createElement('tr');
//         tn = document.createTextNode(testCase);
//         tr.appendChild(tn);
//         for (var testVersion in testVersions){
//             td = document.createElement('td');
//
//             //getResult from testCase and testVersion
//             tn = document.createTextNode();
//             td.appendChild(tn);
//             tr.appendChild(td);
//         }
//         table.appendChild(tr);
//     }
// }



