/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");

// Adds the listeners on page load (exectuted in comments.tmpl)
function initializeCommentsClickListener() {
    // submit comment function
    $("#submitComment").click(function (event) {
        var value = $("#inputCommentField").val();
        if (value) {
            ajaxSendDataToServer (event, window.location.pathname, { commentText: value });
            addCommentToList();
            removeEmptyCommentsLine();
        }
    });

    // Keyboard-Listener
    $("#inputCommentField").onkeypress = updateSubmitCommentButton;
}

// removes the placeholder from the comments list
function removeEmptyCommentsLine() {
    var items = $("#commentListGroup").find(".list-group-item");
    if (items.length === 1) {
        document.getElementById("commentsEmptyLine").remove();
    }
}

// enables or disables the comment submit button
function updateSubmitCommentButton() {
    var submitButton = $("#submitComment");

    //empty text field disables button
    if (!$("#inputCommentField").val()) {
        submitButton.addClass("disabled");
        submitButton.addClass("cursor-not-clickable");
        submitButton.attr("data-original-title", "You have to enter text to make a comment");
    } else {
        // enables button
        submitButton.removeClass("disabled");
        submitButton.removeClass("cursor-not-clickable");
        submitButton.attr("data-original-title", "");
    }
}

// after submitting, a comment is displayed in the front end
function addCommentToList() {
    var commentField = $("#inputCommentField");
    var newCommentText = commentField.val();
    commentField.val("");

    // get's a clone of the template empty comment
    var emptyComment = $("#emptyComment").find(".list-group-item");
    emptyComment = emptyComment.clone(true, true);

    // fills the clone with data
    var commentText = $(emptyComment).find("p");
    $(commentText)[0].innerHTML = newCommentText;
    var commentLabel = $(emptyComment).find("label")[0];
    var userMenu = document.getElementById("userMenu");
    $(commentLabel).text(userMenu.innerText);

    var listGroup = $("#commentListGroup");
    var ind =listGroup.find("li").length;
    emptyComment[0].id = "comment" + ind;

    // adds the comment to the list
    if (ind > 0) {
        $(".comment").first().before(emptyComment);
    } else {
        listGroup.prepend(emptyComment);
    }
}

function sendCommentToServer(event) {
    event.preventDefault();
    var path = currentURL().toString();
    var posting = $.ajax({
        url: path,
        type: "PUT",
        data: {
            commentText: $("#inputCommentField").val()
        }
    });
    posting.done(function (resposne) {
        return true;
    }).fail(function (response) {
        return true;
    });

}