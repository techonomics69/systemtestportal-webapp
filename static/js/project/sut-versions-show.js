/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/


// testCaseVersionData contains the variants and versions of the test case
var testObjectVersionData = {};

// fills Variants into the dropdown
function fillVariants(variants, selectorID) {
    $('#inputTestCaseSUTVariants').empty();
    $.each(variants, function (key, variant) {
        addVariant(variant, selectorID);
    });
}

// adds a variant entry to the dropdown
function addVariant(variant, selectorID) {
    var selector = $(selectorID);
    var selectedValue = selector.val();
    selector.append($("<option></option>")
        .attr("value", variant.Name)
        .text(variant.Name));
    selector.value = selectedValue;
}


// Update the list with versions when selecting another variant
function setVariantOnChangeListener() {
    $('#inputTestCaseSUTVariants').on('change', function () {
        updateVersionShowList()
    });
}

// Update the list with versions when selecting another variant
function setVariantDropdownOnChangeListener(isSequence) {
    $('#inputTestObjectSUTVariants').on('change', function () {
        updateVersionDropdown(testObjectVersionData, "#inputTestObjectSUTVariants", isSequence);
    });
}

// Updates the list with the versions
function updateVersionShowList() {

    var variantKey = $('#inputTestCaseSUTVariants').val();
    if (typeof variantKey == 'undefined'){
        return;
    }
    var list = $('#inputTestCaseSUTVersions');
    // remove all previously shown elements
    list.empty();
    // add versions of selected variant to list
    $.each(testObjectVersionData[variantKey].Versions, function (key, version) {
        var listElement = ($("<li></li>")
            .attr("class", "list-item")
            .html('<span>' + version.Name + '</span>'));
        list.append(listElement)
    });


}

// Updates the list with the versions
function updateVersionDropdown(data, selectorID, isSequence) {
    var variantKey = $('#inputTestObjectSUTVariants').val();
    var list = $('#inputTestObjectSUTVersions');
    // remove all previously shown elements
    list.empty();
    // add versions of selected variant to list
    var versions;
    if(data != null) {
        versions = data[variantKey].Versions;
    }

    $.each(versions, function (key, version) {
        list.append($("<option></option>")
            .attr("value", version.Name)
            .text(version.Name));
    });


}

