/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// Permissions are all permissions that
// a user can have in a project
type Permissions struct {
	ExecutionPermissions
	CasePermissions
	SequencePermissions
	MemberPermissions
	SettingsPermissions
}

// ExecutionPermissions are the permissions
// affecting the execution
type ExecutionPermissions struct {
	Execute bool
}

// CasePermissions are the permissions
// affecting the cases in a project
type CasePermissions struct {
	CreateCase    bool
	EditCase      bool
	DeleteCase    bool
	DuplicateCase bool
	AssignCase    bool
}

// SequencePermissions are the permissions
// affecting the sequences in a project
type SequencePermissions struct {
	CreateSequence    bool
	EditSequence      bool
	DeleteSequence    bool
	DuplicateSequence bool
	AssignSequence    bool
}

// MemberPermissions are the permissions
// affecting the members in a project
type MemberPermissions struct {
	EditMembers bool
}

// SettingsPermissions are the permissions
// affecting the settings of a project
type SettingsPermissions struct {
	EditProject     bool // Change name, description, image, visibility
	DeleteProject   bool
	EditPermissions bool
}

// GetPermissions returns the permissions of a user
// in a project. Returns an empty permission struct
// if the user does not exist in the project.
func (p *Project) GetPermissions(u *user.User) Permissions {
	if p == nil || u == nil {
		return Permissions{}
	}
	r := p.getRole(u)
	if r == nil {
		return Permissions{}
	}
	return r.Permissions
}

// getRole returns the role of a user in a project.
// Returns nil if the user does not exist in the
// project.
func (p *Project) getRole(u *user.User) *Role {
	if p == nil || u == nil {
		return nil
	}
	for _, ms := range p.UserMembers {
		if ms.User.Actor() == u.Name {
			r, ok := p.Roles[ms.Role]
			if !ok {
				return nil
			}
			return r
		}
	}
	return nil
}
